package ist.meic.ssof;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_login);
	        getActionBar().setDisplayHomeAsUpEnabled(true);
	        
	        Bundle bundle = getIntent().getExtras();
	        String user = bundle.getString("user"); 
	        
			((TextView) findViewById(R.id.welcome)).setText("Welcome " + user + "!");
			
			final Button notes = (Button) findViewById(R.id.events_button);
			final Button options = (Button) findViewById(R.id.options_button);

			notes.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, ShowNotesActivity.class);

			        Bundle bundle = getIntent().getExtras();
			        String user = bundle.getString("user"); 
			        boolean adminCheck = bundle.getBoolean("admin?");

					Bundle bundleNew = new Bundle();
					bundleNew.putString("user", user);
					bundleNew.putBoolean("admin?", adminCheck);
                    intent.putExtras(bundleNew);

                    startActivity(intent);
				}
			});
			
			options.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Toast.makeText(getApplicationContext(), "Not implemented yet", Toast.LENGTH_LONG).show();
				}
			});

	    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.activity_login, menu);
	        return true;
	    }

	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	            case android.R.id.home:
	                NavUtils.navigateUpFromSameTask(this);
	                return true;
	        }
	        return super.onOptionsItemSelected(item);
	    }

}
