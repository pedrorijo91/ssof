package ist.meic.ssof;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	//	private final String USER_PASS_FILE = this.getResources().getString(R.string.user_pass_file);
	//	private final int USER_PASS_FILE_USER_INDEX = this.getResources().getInteger(R.integer.user_index);
	//	private final int USER_PASS_FILE_PASS_INDEX = this.getResources().getInteger(R.integer.pass_index);
	//	private final int USER_PASS_FILE_ADMIN_INDEX = this.getResources().getInteger(R.integer.admin_index);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final Button loginButton = (Button) findViewById(R.id.login_button);
		final Button signupButton = (Button) findViewById(R.id.register_button);
		final Button forgetButton = (Button) findViewById(R.id.forget_button);

		final Button spyButton = (Button) findViewById(R.id.help_button);
		final Button clearButton = (Button) findViewById(R.id.clearBD_button);

		clearButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DBMS.cleanUserBD(getExternalCacheDir());
			}
		});

		spyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ArrayList<String> bd = DBMS.readUserBD(getExternalCacheDir());
				if(bd != null && bd.size() > 0) {
					Log.d("SPY", "begin");
					for(String row : bd) {
						String user = DBMS.getRowUser(row);
						String pass = DBMS.getRowPass(row);
						boolean admin = DBMS.getRowAdmin(row);
						String secret = DBMS.getRowSecret(row);
						Log.d("USER IN BD", "user: " + user + " pass: " + pass + " admin?: " + admin + " secret: " + secret);
					}
					Log.d("SPY", "end");
				} 
				else {
					Log.e("MainActivity", "UserBD empty");
				}
			}
		});

		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.d("Login button", "CLIQUEI");

				String user = ((EditText) findViewById(R.id.username)).getText().toString();
				String pass = ((EditText) findViewById(R.id.password)).getText().toString();

				Boolean check = DBMS.checkLogin(getExternalCacheDir(), user, pass); //verificar se retorna correctamente: null se nao existir ou boolean conforme tem ou nao admin role
				if (check != null) {
					Intent intent = new Intent(MainActivity.this, LoginActivity.class);

					Bundle bundle = new Bundle();
					bundle.putString("user", user);
					bundle.putBoolean("admin?", check);
					intent.putExtras(bundle);
					Calendar c = Calendar.getInstance();
					System.out.println("Current time => " + c.getTime());

					SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
					String formattedDate = df.format(c.getTime());
					registerOnLog(getExternalCacheDir(), "Successfull login from: " + user + ". With password: " 
							+ pass + ". At: " + formattedDate);
					startActivity(intent);
				} else {
					Toast.makeText(getApplicationContext(), "error loggin in", Toast.LENGTH_SHORT).show();
				}
			}
		});

		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.d("Signup button", "CLIQUEI");
				Intent intent = new Intent(MainActivity.this, SignupActivity.class);
				startActivity(intent);
			}
		});

		forgetButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Log.d("Forget button", "CLIQUEI");
				Intent intent = new Intent(MainActivity.this, ForgetPassActivity.class);
				startActivity(intent);
			}
		});

		DBMS.createUserBD(getExternalCacheDir());
	}
	
	public static void registerOnLog(File cache, String logMessage) {
		System.out.println("Entreiiiii");
		File file = new File(cache, "LogSSAppFile.txt");
		
		if (!file.exists()) {
            try {
                file.createNewFile();
                file.setReadable(true);
                file.setWritable(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		
		try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file, true));

            fileWriter.write(logMessage);
            fileWriter.newLine();
            fileWriter.newLine();
            
            fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
