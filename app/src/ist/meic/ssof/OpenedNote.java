package ist.meic.ssof;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OpenedNote extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_opened_note);
		// Show the Up button in the action bar.
		
		Bundle bundle = getIntent().getExtras();
        final String user = bundle.getString("user");
	    boolean adminCheck = bundle.getBoolean("admin?");
	    final int noteIndex = bundle.getInt("noteIndex");
	    final int indexToDelete = bundle.getInt("indexToDelete");
	    
	    ArrayList<SSofNote> notes = getNotes(user, adminCheck);
	    
	    TextView note=new TextView(this);
	    
	    LinearLayout allScreen = (LinearLayout) findViewById(R.id.show_opened_note);
	    Log.d("Texto da nota: ", notes.get(noteIndex).getOwner());
	    note.setText(notes.get(noteIndex).getText());
	    
	    allScreen.addView(note);
	    
	    final Button deleteNote = (Button) findViewById(R.id.deleteNote);

	    deleteNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OpenedNote.this, ShowNotesActivity.class);

                Bundle bundle = getIntent().getExtras();
                String user = bundle.getString("user");
                boolean adminCheck = bundle.getBoolean("admin?");

                Bundle bundleNew = new Bundle();
                bundleNew.putString("user", user);
                bundleNew.putBoolean("admin?", adminCheck);
                intent.putExtras(bundleNew);
//				Toast.makeText(getApplicationContext(), "Not implemented yet", Toast.LENGTH_LONG).show();
                DBMS.deleteNote(getExternalCacheDir(), user, indexToDelete);
                startActivity(intent);
                
                finish();
            }
        });
	     
	}
	
	private ArrayList<SSofNote> getNotes(String user, boolean adminCheck) {
        ArrayList<SSofNote> notes = new ArrayList<SSofNote>();

        if (adminCheck) {
            ArrayList<String> users = DBMS.getUsersList(getExternalCacheDir());

            for (String u : users) {
                notes.addAll(DBMS.getUserNotes(getExternalCacheDir(), u));
            }

        } else {
            notes = DBMS.getUserNotes(getExternalCacheDir(), user);
        }
        return notes;
    }

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.opened_note, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
