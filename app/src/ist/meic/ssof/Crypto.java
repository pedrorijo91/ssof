package ist.meic.ssof;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

public class Crypto {

    private static final int KEY_SIZE = 100;

    public static byte[] generateKey(String key) {
        if (key.length() >= 16) {
            key = key.substring(0, 16);
        } else {
            int i = 0;
            while (key.length() < 16) {
                key += key.toCharArray()[i];
                i++;
            }
        }
        return key.getBytes();
    }

    public static String generateXorKey(String pass) {
        if (pass.length() >= KEY_SIZE) {
            return pass.substring(0, KEY_SIZE);
        } else {
            String key = pass;
            int i = 0;
            while (key.length() < KEY_SIZE) {
                int index = i % pass.length();
                key += pass.charAt(index);
                i++;
            }
            return key;
        }

    }

    public static String encrypt(String key, String data) {
        try {
            byte[] keyBytes = generateKey(key);
            byte[] secretBytes = data.getBytes();

            Cipher c = Cipher.getInstance("AES");

            SecretKeySpec k = new SecretKeySpec(keyBytes, "AES");

            c.init(Cipher.ENCRYPT_MODE, k);

            byte[] encryptedDataBytes = c.doFinal(secretBytes);

            String encryptedData = new String(encryptedDataBytes);

            return encryptedData;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String xorEncrypt(String pass, String data) {

        Log.d("Crypto", "pass: " + pass);
        Log.d("Crypto", "text to encrypt: " + data + " - size: " + data.length());
        String key = generateXorKey(pass);
        byte[] keyByte = key.getBytes();
        byte[] dataByte = data.getBytes();

        byte[] encryptedByte = new byte[dataByte.length];
        for (int i = 0; i < dataByte.length; i++) {
            if (i >= keyByte.length) {
                encryptedByte[i] = dataByte[i];
            } else {
                encryptedByte[i] = (byte) (dataByte[i] ^ keyByte[i]);
            }
        }

        String encrypted = new String(encryptedByte);
        Log.d("Crypto.encrypt", "encrypted: " + encrypted);

        return encrypted;
    }

    public static String decrypt(String key, String secret) {
        try {
            byte[] keyBytes = generateKey(key);
            byte[] encrpytedBytes = secret.getBytes();

            Cipher c = Cipher.getInstance("AES");

            SecretKeySpec k = new SecretKeySpec(keyBytes, "AES");

            c.init(Cipher.DECRYPT_MODE, k);

            byte[] decryptedDataBytes = c.doFinal(encrpytedBytes);

            String decryptedData = new String(decryptedDataBytes);

            return decryptedData;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String xorDecrypt(String pass, String secret) {
        Log.d("Crypto", "text to decrypt: " + secret);

        String key = generateXorKey(pass);
        byte[] keyByte = key.getBytes();
        byte[] secretByte = secret.getBytes();

        byte[] decryptedByte = new byte[secretByte.length];
        for (int i = 0; i < secretByte.length; i++) {
            if (i >= keyByte.length) {
                decryptedByte[i] = secretByte[i];
            } else {
                decryptedByte[i] = (byte) (secretByte[i] ^ keyByte[i]);
            }
        }

        String decrypted = new String(decryptedByte);
        System.out.println("decrypted: " + decrypted);

        return decrypted;
    }

}
