package ist.meic.ssof;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		final Button signupButton = (Button) findViewById(R.id.register_button);

		signupButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String user = ((EditText) findViewById(R.id.username)).getText().toString();
				String pass = ((EditText) findViewById(R.id.password)).getText().toString();
				String confirm = ((EditText) findViewById(R.id.confirmPass)).getText().toString();
				String answer = ((EditText) findViewById(R.id.secret_answear)).getText().toString();
				boolean admin = false;

				Log.d("signup-user", user);
				Log.d("signup-pass", pass);
				Log.d("signup-confirm", confirm);
				Log.d("signup-admin?", "" + admin);
				Log.d("signup-answear", answer);

				if (!pass.equals(confirm)) {
					Toast.makeText(getApplicationContext(), "error: password and confirmation doesn't match", Toast.LENGTH_SHORT).show();
					return;
				}

				boolean successful = DBMS.createUser(getExternalCacheDir(), user, pass, admin, answer);

				if (!successful) {
					Toast.makeText(getApplicationContext(), "error: user already exists '" + user + "'", Toast.LENGTH_SHORT).show();
				}
				else {
					Intent intent = new Intent(SignupActivity.this, MainActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_signup, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
