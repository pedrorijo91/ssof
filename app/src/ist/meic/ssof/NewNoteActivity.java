package ist.meic.ssof;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewNoteActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        final Button createNoteButton = (Button) findViewById(R.id.createNoteButton);
        createNoteButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Bundle bundle = getIntent().getExtras();
                String user = bundle.getString("user");
                boolean adminCheck = bundle.getBoolean("admin?");

                String noteText = ((EditText) findViewById(R.id.noteText)).getText().toString();
                SSofNote note = new SSofNote(user, noteText);
                Log.d("Texto para guardar", noteText);

                saveNote(note);

                Intent intent = new Intent(NewNoteActivity.this, ShowNotesActivity.class);
                Bundle bundleNew = new Bundle();
                bundleNew.putString("user", user);
                bundleNew.putBoolean("admin?", adminCheck);
                intent.putExtras(bundleNew);

                startActivity(intent);
                finish();
            }

            private void saveNote(SSofNote note) {
                DBMS.addNote(getExternalCacheDir(), note);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_new_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
