package ist.meic.ssof;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import android.util.Log;

public class DBMS {

    private static final String ADMIN_USER = "root";
    private static final String ADMIN_PASSWORD = "root";

    private final static String USER_PASS_FILE = "userPass.txt";

    private final static int USER_PASS_FILE_USER_INDEX = 0;
    private final static int USER_PASS_FILE_PASS_INDEX = 1;
    private final static int USER_PASS_FILE_ADMIN_INDEX = 2;
    private final static int USER_PASS_FILE_SECRET_INDEX = 3;

    private static final String NOTE_PREFIX = "Notes_";
    private static final String NOTE_EXTENSION = ".txt";
    private static final String NOTE_DELIMITER = "@$%#&$@";

    public static void cleanUserBD(File cache) {
        File file = new File(cache, USER_PASS_FILE);
        file.delete();
        createUserBD(cache);
    }

    public static void createUserBD(File cache) {
        try {
            File file = new File(cache, USER_PASS_FILE);
            if (!file.exists()) {
                file.createNewFile();
                file.setReadable(true);
                file.setWritable(true);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> readUserBD(File cache) {
        try {
            File file = new File(cache, USER_PASS_FILE);
            if (file.exists()) {
                BufferedReader fileBuffer;
                fileBuffer = new BufferedReader(new FileReader(file));
                ArrayList<String> rows = new ArrayList<String>();
                String line = null;

                while ((line = fileBuffer.readLine()) != null) {
                    rows.add(line);
                }
                fileBuffer.close();

                return rows;
            } else {
                Log.e("DBMS", "UserBD DOESNT EXIST");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getRowUser(String row) {
        String[] splited = row.split(":");
        return splited[USER_PASS_FILE_USER_INDEX];
    }

    public static String getRowPass(String row) {
        String[] splited = row.split(":");
        return splited[USER_PASS_FILE_PASS_INDEX];
    }

    public static String getRowSecret(String row) {
        String[] splited = row.split(":");
        return splited[USER_PASS_FILE_SECRET_INDEX];
    }

    public static boolean getRowAdmin(String row) {
        String[] splited = row.split(":");
        return Boolean.parseBoolean(splited[USER_PASS_FILE_ADMIN_INDEX]);
    }

    public static Boolean checkLogin(File cache, String user, String pass) {
        if (user.equals(ADMIN_USER) && pass.equals(ADMIN_PASSWORD)) {
            return true;
        }

        try {
            File file = new File(cache, USER_PASS_FILE);

            if (file.exists()) {
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
                String line = null;

                while ((line = fileBuffer.readLine()) != null) {
                    if (getRowUser(line).equals(user) && getRowPass(line).equals(pass)) {
                        fileBuffer.close();
                        return getRowAdmin(line);
                    }
                }
                fileBuffer.close();
                return null;
            } else {
                Log.e("DBMS", "UserBD DOESNT EXIST");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean createUser(File cache, String user, String pass, boolean admin, String answer) {
        try {
            File file = new File(cache, USER_PASS_FILE);

            if (file.exists()) {
                boolean userAllowed = checkUserAvailability(file, user);

                if (!userAllowed) {
                    Log.d("DBMS: createUser", "error: user already exists '" + user + "'");
                    return false;
                } else {
                    BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file, true));
                    String newUser = user + ":" + pass + ":" + admin + ":" + answer;
                    fileWriter.write(newUser);
                    fileWriter.newLine();
                    fileWriter.close();
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkUserAvailability(File bd, String user) {
        try {
            BufferedReader fileReader;
            fileReader = new BufferedReader(new FileReader(bd));
            String line = null;

            while ((line = fileReader.readLine()) != null) {
                if (getRowUser(line).equals(user)) {
                    fileReader.close();
                    return false;
                }
            }
            fileReader.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkSecret(File cache, String user, String secret) {
        try {
            File file = new File(cache, USER_PASS_FILE);
            if (file.exists()) {
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
                String line = null;

                while ((line = fileBuffer.readLine()) != null) {
                    if (getRowUser(line).equals(user) && getRowSecret(line).equals(secret)) {
                        Log.w("Successful recovery", "user: " + user + " secret: " + secret);
                        fileBuffer.close();
                        return true;
                    }
                }
                fileBuffer.close();
                return false;
            } else {
                Log.e("DBMS: checkSecret", "UserBD DOESNT EXIST");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    

    public static void changeUserPassword(File cache, String user, String pass) {

        try {
            File file = new File(cache, USER_PASS_FILE);
            File newFile = new File(cache, "NEW" + USER_PASS_FILE);
            if (file.exists()) {
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
                BufferedWriter fileWriter = new BufferedWriter(new FileWriter(newFile, false));

                String line = null;

                while ((line = fileBuffer.readLine()) != null) {
                    if (getRowUser(line).equals(user)) {

                        String answer = getRowSecret(line);
                        boolean admin = getRowAdmin(line);

                        Log.d("signup-user", user);
                        Log.d("signup-pass", pass);
                        Log.d("signup-admin?", "" + admin);
                        Log.d("signup-answear", answer);

                        String newUser = user + ":" + pass + ":" + admin + ":" + answer;
                        fileWriter.write(newUser);
                        fileWriter.newLine();

                    } else {
                        fileWriter.write(line);
                        fileWriter.newLine();
                    }
                }
                fileBuffer.close();
                fileWriter.close();
                file.delete();
                boolean b = newFile.renameTo(new File(cache, USER_PASS_FILE));
                Log.d("DBMS: ChangePassword", "changed file renamed to: " + newFile.getAbsolutePath() + " successful? " + b);
            } else {
                Log.e("DBMS: changePassword", "UserBD DOESNT EXIST");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getUsersList(File cache) {
        ArrayList<String> users = new ArrayList<String>();
        try {
            File file = new File(cache, USER_PASS_FILE);
            if (file.exists()) {
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
                String line = null;

                while ((line = fileBuffer.readLine()) != null) {
                    users.add(getRowUser(line));
                }

                fileBuffer.close();
                //file.delete();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return users;
    }

    /************************ Notes section ************************/
    public static void addNote(File cache, SSofNote note) {
        File file = createNotesFile(cache, note);

        try {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file, true));

            String encryptedData = SimpleCrypto.encrypt(note.getOwner(), note.getText());

            fileWriter.write(encryptedData);
            fileWriter.newLine();
            fileWriter.write(NOTE_DELIMITER);
            fileWriter.newLine();
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch(Exception e) {
        	e.printStackTrace();
        }
    }
    
    public static void deleteAllNotes(File cache, String user, boolean adminCheck) {
    	if(adminCheck) {
    		ArrayList<String> users = getUsersList(cache);
    		
    		 for (String u : users) {
    			 File file = new File(cache, NOTE_PREFIX + u + NOTE_EXTENSION);
    		    	if (file.exists()) {
    		    		file.delete();
    		    	}
             }
    	} else {
	    	File file = new File(cache, NOTE_PREFIX + user + NOTE_EXTENSION);
	    	if (file.exists()) {
	    		file.delete();
	    	}
    	}
    	
    }

    public static File createNotesFile(File cache, SSofNote note) {
        File file = new File(cache, NOTE_PREFIX + note.getOwner() + NOTE_EXTENSION);

        if (!file.exists()) {
            try {
                file.createNewFile();
                file.setReadable(true);
                file.setWritable(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
    
    public static void deleteNote(File cache, String user, int index) {
    	File tmpfile = new File(cache, "tmp.txt");

        if (!tmpfile.exists()) {
            try {
            	tmpfile.createNewFile();
            	tmpfile.setReadable(true);
            	tmpfile.setWritable(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        File file = new File(cache, NOTE_PREFIX + user + NOTE_EXTENSION);
        if (file.exists()) {
        	try {
				BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(tmpfile, true));
				String line = null;
				int counter = 0;
                
                while ((line = fileBuffer.readLine()) != null) {
                    if (line.equals(NOTE_DELIMITER)) {
                    	if(counter != index) {
                    		fileWriter.write(line);
                            fileWriter.newLine();
                    	}
                    	counter++;
                    } else {
                    	if(counter != index) {
                    		fileWriter.write(line);
                            fileWriter.newLine();
                    	}
                    }
                }
                fileBuffer.close();
                fileWriter.close();
                tmpfile.renameTo(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

    public static ArrayList<SSofNote> getUserNotes(File cache, String user) {
        ArrayList<SSofNote> notes = new ArrayList<SSofNote>();

        File file = new File(cache, NOTE_PREFIX + user + NOTE_EXTENSION);
        if (file.exists()) {
            try {
                BufferedReader fileBuffer = new BufferedReader(new FileReader(file));
                String line = null;
                String appendedNote = new String();

                while ((line = fileBuffer.readLine()) != null) {
                    if (line.equals(NOTE_DELIMITER)) {
                        String text = SimpleCrypto.decrypt(user, appendedNote);
                        appendedNote = new String();

                        SSofNote note = new SSofNote(user, text);
                        notes.add(note);
                    } else {
                        appendedNote += "\n" + line;
                    }
                }
                fileBuffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch(Exception e) {
            	e.printStackTrace();
            }
        }

        return notes;

    }

}
