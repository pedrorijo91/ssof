package ist.meic.ssof;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ForgetPassActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        final Button changeButton = (Button) findViewById(R.id.change_pass_button);
        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Change pass button", "CLIQUEI");

                String user = ((EditText) findViewById(R.id.username)).getText().toString();
                String secret = ((EditText) findViewById(R.id.answear)).getText().toString();

                boolean check = DBMS.checkSecret(getExternalCacheDir(), user, secret);
                if (check) {
                    Intent intent = new Intent(ForgetPassActivity.this, ChangePasswordActivity.class);

                    Bundle bundle = new Bundle();
                    bundle.putString("user", user);
                    intent.putExtras(bundle);

                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "error: username and secret mismatch", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_forget_pass, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
