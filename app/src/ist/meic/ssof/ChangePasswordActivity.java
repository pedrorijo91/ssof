package ist.meic.ssof;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePasswordActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);
		getActionBar().setDisplayHomeAsUpEnabled(true);

		final Button okButton = (Button) findViewById(R.id.confirm_button);
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				String pass = ((EditText) findViewById(R.id.new_pass)).getText().toString();
				String confirmation = ((EditText) findViewById(R.id.new_pass_confirmation)).getText().toString();

				if (pass.equals(confirmation)) {
					String user = getIntent().getStringExtra("user");
					DBMS.changeUserPassword(getExternalCacheDir(), user, pass);
				} else {
					Toast.makeText(getApplicationContext(), "error: password and confirmation mismatch", Toast.LENGTH_SHORT).show();
				}

				Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_change_password, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
