package ist.meic.ssof;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ShowNotesActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        super.onCreate(savedInstanceState);
        final Bundle toButton = savedInstanceState;
        setContentView(R.layout.activity_show_notes);
        getActionBar().setDisplayHomeAsUpEnabled(true);
     

        Bundle bundle = getIntent().getExtras();
        final String user = bundle.getString("user");
        final boolean adminCheck = bundle.getBoolean("admin?");
   
        ArrayList<SSofNote> notes = getNotes(user, adminCheck);
       
        int nEvents = notes.size();
        /*
         * mostrar eventos em lista:
         http://www.vogella.com/articles/AndroidListView/article.html 
          http://developer.android.com/guide/topics/ui/layout/listview.html
         * quando clicar mostrar detalhes do evento e opccao de editar
         */
        Log.d("ShowNotes - User: " + user + " admin? " + adminCheck, "" + nEvents);

        final Button createNote = (Button) findViewById(R.id.createNoteButton);
        createNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowNotesActivity.this, NewNoteActivity.class);

                Bundle bundle = getIntent().getExtras();
                String user = bundle.getString("user");
                boolean adminCheck = bundle.getBoolean("admin?");

                Bundle bundleNew = new Bundle();
                bundleNew.putString("user", user);
                bundleNew.putBoolean("admin?", adminCheck);
                intent.putExtras(bundleNew);
//				Toast.makeText(getApplicationContext(), "Not implemented yet", Toast.LENGTH_LONG).show();

                startActivity(intent);
                finish();
            }
        });
        
        final Button deleteAll = (Button) findViewById(R.id.deleteAllButton);

        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(!adminCheck) {
                	DBMS.deleteAllNotes(getExternalCacheDir(), user, adminCheck);
                	onCreate(toButton);
                //}
            }
        });
        
        // create list of notes available
        LinearLayout allScreen = (LinearLayout) findViewById(R.id.show_notes_screen);
        
        int firstOfThisUser = 0;
        String lastUser = "";
        for(int i = 0; i < nEvents; i++) {
        	Button newButton = new Button(this);
        	newButton.setText("Note " + i);
        	RelativeLayout minScreen = new RelativeLayout(this);
        	minScreen.addView(newButton);
        	allScreen.addView(minScreen);
        	if(!lastUser.equals(notes.get(i).getOwner())) {
        		lastUser = notes.get(i).getOwner();
        		firstOfThisUser = i;
        	}
        	final int indexToDelete = i - firstOfThisUser;
        	final int index = i;
        	final String owner = notes.get(i).getOwner();
        	
        	newButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ShowNotesActivity.this, OpenedNote.class);

                    Bundle bundle = getIntent().getExtras();
                    boolean adminCheck = bundle.getBoolean("admin?");

                    Bundle bundleNew = new Bundle();
                    bundleNew.putString("user", owner);
                    bundleNew.putBoolean("admin?", adminCheck);
                    bundleNew.putInt("noteIndex", index);
                    bundleNew.putInt("indexToDelete", indexToDelete);
                    intent.putExtras(bundleNew);
//    				Toast.makeText(getApplicationContext(), "Not implemented yet", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                    finish();
                }
            });
        }
    }

    private ArrayList<SSofNote> getNotes(String user, boolean adminCheck) {
        ArrayList<SSofNote> notes = new ArrayList<SSofNote>();

        if (adminCheck) {
            ArrayList<String> users = DBMS.getUsersList(getExternalCacheDir());
      
            for (String u : users) {
                notes.addAll(DBMS.getUserNotes(getExternalCacheDir(), u));
            }
        } else {
            notes = DBMS.getUserNotes(getExternalCacheDir(), user);
        }
        return notes;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_show_events, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
