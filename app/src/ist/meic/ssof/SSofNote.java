package ist.meic.ssof;

public class SSofNote {
	
	private String owner;
	private String text;
//	private String date;
	
	public SSofNote(String owner, String text) {
		this.owner = owner;
		this.text = text;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public String getText() {
		return text;
	}
	
}
