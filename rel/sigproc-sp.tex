\documentclass{acm_proc_article-sp}
\usepackage[utf8]{inputenc}
\usepackage{enumitem}
\usepackage{fancyref}
\usepackage{hyperref}


\begin{document}

\title{An Android vulnerable application for managing personal notes}

\numberofauthors{2} 
%
\author{
\alignauthor
Miguel Aragão\\
       \affaddr{Técnico Lisboa}\\
       \affaddr{Avenida Rovisco Pais, 1}\\
       \affaddr{1049-001 Lisbon}\\
       \affaddr{Lisbon, Portugal}\\
       \email{miguel.aragao@tecnico.ulisboa.pt}
\alignauthor
Pedro Rijo\\
       \affaddr{Técnico Lisboa}\\
       \affaddr{Avenida Rovisco Pais, 1}\\
       \affaddr{1049-001 Lisbon}\\
       \affaddr{Lisbon, Portugal}\\
       \email{pedro.rijo@tecnico.ulisboa.pt}
}

\maketitle
\begin{abstract}
Since iPhone debut on June 29, 2007\footnote{http://www.macworld.com/article/1054769/iphone.html}, mobile market has benefited from an explosion. With Apple leading this revolutionized market, Google released Android mobile operating system, an easier and less restrained system for developers. Google's approach lead to the increase of third-party applications for mobile systems. Like any other software mobile, applications contain security issues. With this scenario OWASP\footnote{https://www.owasp.org} added to the already existent annual top of security risks for web applications\footnote{https://www.owasp.org/index.php/Top\_10\_2013-Top\_10} a top 10 mobile risks available at \url{https://www.owasp.org/index.php/OWASP\_Mobile\_Security\_Project\#tab=Top\_Ten\_Mobile\_Risks}.
This work pretends to present some of this risks releasing a vulnerable Android application and making a survey on it.

\end{abstract}

\keywords{Security, Mobile} 

\section{Introduction}
Software security has been a major concern in recent past due to some expensive software errors like NASA Mars Lander which suffered a units conversion bug or NASA Mars Pathfinder that had a priority-inversion bug.
With the rising number of applications developed for mobile systems by "amateurs" programmers, mobile security started to become more important. 

Many of the same mistakes made over the past decade in other areas of application security have managed to resurface in the mobile world. There have also been many new security challenges introduced by mobile applications and platforms. Through the OWASP Mobile Security Project, the primary goal is to enhance the visibility of mobile security risks just as OWASP has successfully done for the web, helping developers and organizations prioritize their security efforts throughout the development life cycle.

As the attack surface and threat landscape for mobile applications continues to rapidly evolve, arming developers with the tools they need to succeed is essential. Each environment presents very unique and different risks to consider as exposed on OWASP AppSecUSA 2011\footnote{http://www.youtube.com/watch?v=GRvegLOrgs0}

\section{OWASP Top 10 Mobile Risks}
The top elaborated by OWASP intends to be platform-agnostic and focused on areas of risk rather than individual vulnerabilities. Weighted utilizing OWASP Risk Rating Methodology\footnote{https://www.owasp.org/index.php/OWASP\_Risk\_Rating\_Methodology} it contains the following risks:

\begin{enumerate}[label=\bfseries M\arabic*:]
	\item Insecure Data Storage
	\item Weak Server Side Controls 
	\item Insufficient Transport Layer Protection
	\item Client Side Injection
	\item Poor Authorization and Authentication
	\item Improper Session Handling
	\item Security Decisions Via Untrusted Inputs
	\item Side Channel Data Leakage
	\item Broken Cryptography
	\item Sensitive Information Disclosure
\end{enumerate}

\subsection{M1 - Insecure Data Storage}
The first risk applies to locally stored data and cloud synchronization, and is defined by letting sensitive data unprotected. It results generally of not encrypting data, caching data not intended for long-term storage, weak or global permissions, or not leveraging platform best-pratices.

It has impact on confidentially of data lost, credentials disclosed, privacy violations and non-compliance.

As examples we have credentials stored in a world readable file.

To prevent it one should store only what is absolutely required, never use public storage areas like SD card, leverage secure containers and platform provided file encryption APIs, and do not grant files wold reabable/writeable permissions.

\subsection{M2 - Weak Server Side Controls}
This risks applies to back-end services used by the application and are not mobile specific. Controls shouldn't trust the client and may need to be re-evaluated. It may lead for example to SQL injection attacks. This kind of vulnerability was present on first iPad release.

It has impact on the losing of confidentially of data, and it's due to the integrity of data can't trusted.

To prevent one should always sanitize input.

\subsection{M3 - Insufficient Transport Layer protection}
This third risk is due to problems on encryption of transmitted data allowing man-in-the-middle attacks, tampering with data in transit and leading to confidentiality of data lost. This happened on Google ClientLogin Authentication Protocol where a token was sent to automatically synchronize data from the server when users connected via Wi-Fi.

In order to protect from this vulnerability the developer must ensure that all sensitive data leaving the device is encrypted using SSL for example.

\subsection{M4 - Client Side Injection}
This problem is also present in web applications top risks and is widely known. In mobile devices is due to application using browser libraries for example and may allow to the abuse by the attacker of phone dialer and SMS or in-app payments. It leads to the compromise of the device, toll fraud and privilege escalation.

As usual, to prevent it, the input must be sanitized or escaped before rendering or executing it. In calls to databases it should be used prepared statements to avoid concatenation. Also it should minimize the sensitive capabilities tied to hybrid web functionality.

\subsection{M5 - Poor Authorization and Authentication}
This problem has in part relation with mobile and in other part relation with the architecture.
Some applications rely solely on immutable and potentially compromised valued like IMEI, IMSI and UUID. The hardware identifiers persist across data wipes, factory resets and the change of owner. Privilege escalation and unauthorized access may happen from this vulnerability.

To prevent this attack one shouldn't use contextual information as sole piece of authentication data.

\subsection{M6 - Improper Session Handling}
Mobile application sessions are generally much longer due to convenience and usability. The use of a device identifier as a session token may lead to privilege escalation, unauthorized access and to circumvent licensing and payments.

Programmers shouldn't be afraid of make users to re-authenticate every so often. They should also ensure that tokens can be revoked quickly in the event of a lost/stolen device. 

\subsection{M7 - Security Decisions Via Untrusted Inputs}
This vulnerability can be leveraged to bypass permissions and security models. It's similar but it depends on the platform. For example, in Android it consists in abusing intents. As a result it can consume paid resources, it may lead to data exfiltration and privilege escalation. The Skype application suffered from this issue.

The application should check caller's permissions at input boundaries and prompt the user for additional authorization before allowing it. If permissions checks cannot be performed it must be ensured that additional steps are required to launch sensitive actions.

\subsection{M8 - Side Channel Data Leakage}
Side channel data leakage occurs due to a mix of not disabling platform features and programmatic flaws. It leads to sensitive data end up in unintended places like cache, keystroke logging, screenshots, logs and temporary directories resulting in data retained indefinitely and privacy violations.

It's crucial to understand what are third-party libraries doing with user data. Also, one should never log credentials or other sensitive data, should remove sensitive data before screenshots, disable keystroke logging per field and use anti-caching directives.

\subsection{M9 - Broken Cryptography}
Encoding, obfuscation and serialization are often confused with encryption, but they are pretty different things. 
This confusion may result in confidentiality of data lost, privilege escalation and circumvent of business logic. Another source of problem is the use of weak algorithm, for example symmetric encryption with a weak encryption key.

To avoid the attack one should use crypto libraries already tested and take advantage of protections provided by the platform.

\subsection{M10 Sensitive Information Disclosure}
Although similar to M1, it has some subtle differences. Instead of stored, information is embedded or hardcoded. With reverse engineering on the application this situation it may be exploited. Although code obfuscation helps, it does not eliminate the risk. It leads to disclosure of credentials and exposition of intellectual property.

Prevention is possible keeping that kind of information out of the client. Proprietary and sensitive business logic on the server, and never hardcoded a password.

\section{The Application}
The developed application is a simple application that allows saving important notes for each local user, similar to Evernote\footnote{www.evernote.com} but simpler, without many of its features and without synchronization.

Each local user can signup, change is password and manage his notes. It is allowed to create new notes, see them, and delete them. There is a default root user with admin privileges that can manage everyone's notes.

%\begin{figure}[ht!]
%\centering
%\includegraphics[width=30mm]{Figures/login.png}
%\caption{Application screenshot of login menu}
%\label{figure:login}
%\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=30mm]{Figures/signup.png}
\caption{Application screenshot of signup menu}
\label{figure:signup}
\end{figure}

%\begin{figure}[ht!]
%\centering
%\includegraphics[width=30mm]{Figures/main.png}
%\caption{Application screenshot of main menu}
%\label{figure:main}
%\end{figure}

\subsection{Application Vulnerabilities}
\label{subsection:applicationVulnerabilities}

The developed application contains some of the described vulnerabilities in order to demonstrate them and how to fix them. Next we present those inserted vulnerabilities.

\subsubsection{M1 - Insecure Data Storage}
\label{subsubsection:m1}
The applications saves the username and password information on a plain text file in SD card with world permissions. This allows for an attacker manually, or through another application, to access the file and read it.

\subsubsection{M4 - Client Side Injection}
Users may register for a new account on the application. The input from chosen username and password is not being verified allowing attacks. If an attacker has access to the information of users database it may inject a delimiter and give admin permissions that are by default set to false to that account. As example an attacker may insert as password $"anypassword:true"$, giving himself admin privileges.

\subsubsection{M8 - Side Channel Data Leakage}
The developed applications suffers from data leakage due to the fact that sensitive data is being logged. When a user executes login the credentials are being saved to the logger allowing an attacker to look for the log file in the platform and having access to private information.

\subsubsection{M9 - Broken Cryptography}
User notes are stored in files with the text encrypted, but the encryption is made using a simmetric algorithm that used the user as the key. If an attacker knew this detail would easily decrypt the information having full access to private notes.

\subsubsection{M10 - Sensitive Information Disclosure}
The application contains an hardcoded admin by default. The user "root" with password "root" gives access to every user's notes. If an attacker tries to login with that account it may get access to private information of all users.

\subsection{Proposed Corrections}

In this section we present our proposed corrections to the vulnerabilities presented on Sec.\ref{subsection:applicationVulnerabilities}. This may not be the only option for correcting those issues but are intended to be a guideline.

\subsubsection{M1 - Insecure Data Storage}
On Sec.\ref{subsubsection:m1} we explained why the application contained this vulnerability. 
For the issue of saving the file in SD card is enough to specify other location, like mobile platform internal storage area. The user's information shouldn't neither to be in a plain text, and a local database could prevent this problem. Regarding the world permissions of the file, the database solution could solve this problem but if one chose to save in a plain text it should at least constrain the file permissions and set storage encryption.

\subsubsection{M4 - Client Side Injection}
In order to prevent privilege escalation for an user it may be enough to sanitizing the input detecting if the input contains the delimiters used by the application, in this case the character ":".

\subsubsection{M8 - Side Channel Data Leakage}
To avoid the problem discussed it is enough to remove the sending of login information to the log file, or encrypt the log file.

\subsubsection{M9 - Broken Cryptographic}
To correct the problem of broken cryptographic there are some alternatives. On the first solution the key used could be improved with some randomness. Another solution is to use a stronger algorithm, probably an asymmetric approach.

\subsubsection{M10 - Sensitive Information Disclosure}
To prevent the problem raised by the hardcoded admin account, that code should be removed and possibly replacing it by setting the first user to signup as admin.

\section{Conclusions}
As showed with this application, mobile security can easily be compromised without the appropriate attention on some details. But, fortunately, one can prevent them usually with little effort on it.
The lack of attention on those subtle details can lead to a vulnerable application that will thus lead to many issues such as privacy invasion, data confidentiality loss, credentials disclosure, privilege escalation and many others as referred on previous sections.

Just like usual security in  web applications, security on mobile applications has become more important each day with the dissemination of mobile platforms and the increasing number of non-professional developers and available applications, sometimes with unknown source. Mobile applications tend to use personal information and credentials to synchronize through a set of devices and so become a target of software attacks. With this in mind programmers should always concern to possible failures that can cause software vulnerabilities.

\end{document}

\section{The {\secit Body} of The Paper}
Typically, the body of a paper is organized
into a hierarchical structure, with numbered or unnumbered
headings for sections, subsections, sub-subsections, and even
smaller sections.  The command \texttt{{\char'134}section} that
precedes this paragraph is part of such a
hierarchy.\footnote{This is the second footnote.  It
	starts a series of three footnotes that add nothing
		informational, but just give an idea of how footnotes work
		and look. It is a wordy one, just so you see
		how a longish one plays out.} \LaTeX\ handles the numbering
		and placement of these headings for you, when you use
		the appropriate heading commands around the titles
		of the headings.  If you want a sub-subsection or
		smaller part to be unnumbered in your output, simply append an
		asterisk to the command name.  Examples of both
		numbered and unnumbered headings will appear throughout the
		balance of this sample document.

		Because the entire article is contained in
		the \textbf{document} environment, you can indicate the
		start of a new paragraph with a blank line in your
		input file; that is why this sentence forms a separate paragraph.

		\subsection{Type Changes and {\subsecit Special} Characters}
		We have already seen several typeface changes in this sample.  You
		can indicate italicized words or phrases in your text with
		the command \texttt{{\char'134}textit}; emboldening with the
		command \texttt{{\char'134}textbf}
		and typewriter-style (for instance, for computer code) with
		\texttt{{\char'134}texttt}.  But remember, you do not
		have to indicate typestyle changes when such changes are
		part of the \textit{structural} elements of your
		article; for instance, the heading of this subsection will
		be in a sans serif\footnote{A third footnote, here.
			Let's make this a rather short one to
				see how it looks.} typeface, but that is handled by the
				document class file. Take care with the use
				of\footnote{A fourth, and last, footnote.}
				the curly braces in typeface changes; they mark
				the beginning and end of
				the text that is to be in the different typeface.

				You can use whatever symbols, accented characters, or
				non-English characters you need anywhere in your document;
				you can find a complete list of what is
				available in the \textit{\LaTeX\
					User's Guide}\cite{Lamport:LaTeX}.

					\subsection{Math Equations}
					You may want to display math equations in three distinct styles:
					inline, numbered or non-numbered display.  Each of
					the three are discussed in the next sections.

					\subsubsection{Inline (In-text) Equations}
					A formula that appears in the running text is called an
					inline or in-text formula.  It is produced by the
					\textbf{math} environment, which can be
					invoked with the usual \texttt{{\char'134}begin. . .{\char'134}end}
					construction or with the short form \texttt{\$. . .\$}. You
					can use any of the symbols and structures,
					from $\alpha$ to $\omega$, available in
					\LaTeX\cite{Lamport:LaTeX}; this section will simply show a
					few examples of in-text equations in context. Notice how
					this equation: \begin{math}\lim_{n\rightarrow \infty}x=0\end{math},
					set here in in-line math style, looks slightly different when
					set in display style.  (See next section).

					\subsubsection{Display Equations}
					A numbered display equation -- one set off by vertical space
					from the text and centered horizontally -- is produced
					by the \textbf{equation} environment. An unnumbered display
					equation is produced by the \textbf{displaymath} environment.

					Again, in either environment, you can use any of the symbols
					and structures available in \LaTeX; this section will just
					give a couple of examples of display equations in context.
					First, consider the equation, shown as an inline equation above:
					\begin{equation}\lim_{n\rightarrow \infty}x=0\end{equation}
					Notice how it is formatted somewhat differently in
					the \textbf{displaymath}
					environment.  Now, we'll enter an unnumbered equation:
					\begin{displaymath}\sum_{i=0}^{\infty} x + 1\end{displaymath}
					and follow it with another numbered equation:
					\begin{equation}\sum_{i=0}^{\infty}x_i=\int_{0}^{\pi+2} f\end{equation}
					just to demonstrate \LaTeX's able handling of numbering.

					\subsection{Citations}
					Citations to articles \cite{bowman:reasoning, clark:pct, braams:babel, herlihy:methodology},
					conference
					proceedings \cite{clark:pct} or books \cite{salas:calculus, Lamport:LaTeX} listed
					in the Bibliography section of your
					article will occur throughout the text of your article.
					You should use BibTeX to automatically produce this bibliography;
					you simply need to insert one of several citation commands with
					a key of the item cited in the proper location in
					the \texttt{.tex} file \cite{Lamport:LaTeX}.
					The key is a short reference you invent to uniquely
					identify each work; in this sample document, the key is
					the first author's surname and a
					word from the title.  This identifying key is included
					with each item in the \texttt{.bib} file for your article.

					The details of the construction of the \texttt{.bib} file
					are beyond the scope of this sample document, but more
					information can be found in the \textit{Author's Guide},
					and exhaustive details in the \textit{\LaTeX\ User's
						Guide}\cite{Lamport:LaTeX}.

						This article shows only the plainest form
						of the citation command, using \texttt{{\char'134}cite}.
						This is what is stipulated in the SIGS style specifications.
						No other citation format is endorsed.

						\subsection{Tables}
						Because tables cannot be split across pages, the best
						placement for them is typically the top of the page
						nearest their initial cite.  To
						ensure this proper ``floating'' placement of tables, use the
						environment \textbf{table} to enclose the table's contents and
						the table caption.  The contents of the table itself must go
						in the \textbf{tabular} environment, to
						be aligned properly in rows and columns, with the desired
						horizontal and vertical rules.  Again, detailed instructions
						on \textbf{tabular} material
						is found in the \textit{\LaTeX\ User's Guide}.

						Immediately following this sentence is the point at which
						Table 1 is included in the input file; compare the
						placement of the table here with the table in the printed
						dvi output of this document.

						\begin{table}
						\centering
						\caption{Frequency of Special Characters}
						\begin{tabular}{|c|c|l|} \hline
						Non-English or Math&Frequency&Comments\\ \hline
						\O & 1 in 1,000& For Swedish names\\ \hline
						$\pi$ & 1 in 5& Common in math\\ \hline
						\$ & 4 in 5 & Used in business\\ \hline
						$\Psi^2_1$ & 1 in 40,000& Unexplained usage\\
							\hline\end{tabular}
							\end{table}

							To set a wider table, which takes up the whole width of
							the page's live area, use the environment
							\textbf{table*} to enclose the table's contents and
							the table caption.  As with a single-column table, this wide
							table will ``float" to a location deemed more desirable.
							Immediately following this sentence is the point at which
							Table 2 is included in the input file; again, it is
							instructive to compare the placement of the
							table here with the table in the printed dvi
							output of this document.


							\begin{table*}
							\centering
							\caption{Some Typical Commands}
							\begin{tabular}{|c|c|l|} \hline
							Command&A Number&Comments\\ \hline
							\texttt{{\char'134}alignauthor} & 100& Author alignment\\ \hline
							\texttt{{\char'134}numberofauthors}& 200& Author enumeration\\ \hline
							\texttt{{\char'134}table}& 300 & For tables\\ \hline
							\texttt{{\char'134}table*}& 400& For wider tables\\ \hline\end{tabular}
							\end{table*}
							% end the environment with {table*}, NOTE not {table}!

							\subsection{Figures}
							Like tables, figures cannot be split across pages; the
							best placement for them
							is typically the top or the bottom of the page nearest
							their initial cite.  To ensure this proper ``floating'' placement
							of figures, use the environment
							\textbf{figure} to enclose the figure and its caption.

							This sample document contains examples of \textbf{.eps}
							and \textbf{.ps} files to be displayable with \LaTeX.  More
							details on each of these is found in the \textit{Author's Guide}.

							\begin{figure}
							\centering
							\epsfig{file=fly.eps}
							\caption{A sample black and white graphic (.eps format).}
							\end{figure}

							\begin{figure}
							\centering
							\epsfig{file=fly.eps, height=1in, width=1in}
\caption{A sample black and white graphic (.eps format)
	that has been resized with the \texttt{epsfig} command.}
	\end{figure}


	As was the case with tables, you may want a figure
	that spans two columns.  To do this, and still to
	ensure proper ``floating'' placement of tables, use the environment
	\textbf{figure*} to enclose the figure and its caption.

	Note that either {\textbf{.ps}} or {\textbf{.eps}} formats are
	used; use
	the \texttt{{\char'134}epsfig} or \texttt{{\char'134}psfig}
	commands as appropriate for the different file types.

	\subsection{Theorem-like Constructs}
	Other common constructs that may occur in your article are
	the forms for logical constructs like theorems, axioms,
	corollaries and proofs.  There are
	two forms, one produced by the
	command \texttt{{\char'134}newtheorem} and the
	other by the command \texttt{{\char'134}newdef}; perhaps
	the clearest and easiest way to distinguish them is
	to compare the two in the output of this sample document:

	This uses the \textbf{theorem} environment, created by
	the\linebreak\texttt{{\char'134}newtheorem} command:
	\newtheorem{theorem}{Theorem}
	\begin{theorem}
	Let $f$ be continuous on $[a,b]$.  If $G$ is
	an antiderivative for $f$ on $[a,b]$, then
	\begin{displaymath}\int^b_af(t)dt = G(b) - G(a).\end{displaymath}
	\end{theorem}

	The other uses the \textbf{definition} environment, created
	by the \texttt{{\char'134}newdef} command:
	\newdef{definition}{Definition}
	\begin{definition}
	If $z$ is irrational, then by $e^z$ we mean the
	unique number which has
	logarithm $z$: \begin{displaymath}{\log e^z = z}\end{displaymath}
	\end{definition}

	%\begin{figure}
	%\centering
	%\psfig{file=rosette.ps, height=1in, width=1in,}
	%\caption{A sample black and white graphic (.ps format) that has
		%been resized with the \texttt{psfig} command.}
		%\end{figure}

		Two lists of constructs that use one of these
		forms is given in the
		\textit{Author's  Guidelines}.

		\begin{figure*}
		\centering
		\epsfig{file=flies.eps}
\caption{A sample black and white graphic (.eps format)
	that needs to span two columns of text.}
	\end{figure*}
	and don't forget to end the environment with
{figure*}, not {figure}!

There is one other similar construct environment, which is
already set up
for you; i.e. you must \textit{not} use
a \texttt{{\char'134}newdef} command to
create it: the \textbf{proof} environment.  Here
is a example of its use:
\begin{proof}
Suppose on the contrary there exists a real number $L$ such that
\begin{displaymath}
\lim_{x\rightarrow\infty} \frac{f(x)}{g(x)} = L.
\end{displaymath}
Then
\begin{displaymath}
l=\lim_{x\rightarrow c} f(x)
	= \lim_{x\rightarrow c}
	\left[ g{x} \cdot \frac{f(x)}{g(x)} \right ]
	= \lim_{x\rightarrow c} g(x) \cdot \lim_{x\rightarrow c}
	\frac{f(x)}{g(x)} = 0\cdot L = 0,
	\end{displaymath}
	which contradicts our assumption that $l\neq 0$.
	\end{proof}

	Complete rules about using these environments and using the
	two different creation commands are in the
	\textit{Author's Guide}; please consult it for more
	detailed instructions.  If you need to use another construct,
	not listed therein, which you want to have the same
	formatting as the Theorem
	or the Definition\cite{salas:calculus} shown above,
	use the \texttt{{\char'134}newtheorem} or the
	\texttt{{\char'134}newdef} command,
	respectively, to create it.

	\subsection*{A {\secit Caveat} for the \TeX\ Expert}
	Because you have just been given permission to
	use the \texttt{{\char'134}newdef} command to create a
	new form, you might think you can
	use \TeX's \texttt{{\char'134}def} to create a
	new command: \textit{Please refrain from doing this!}
	Remember that your \LaTeX\ source code is primarily intended
	to create camera-ready copy, but may be converted
	to other forms -- e.g. HTML. If you inadvertently omit
	some or all of the \texttt{{\char'134}def}s recompilation will
	be, to say the least, problematic.

	%\end{document}  % This is where a 'short' article might terminate

	%ACKNOWLEDGMENTS are optional
	\section{Acknowledgments}
	This section is optional; it is a location for you
	to acknowledge grants, funding, editing assistance and
	what have you.  In the present case, for example, the
	authors would like to thank Gerald Murray of ACM for
	his help in codifying this \textit{Author's Guide}
	and the \textbf{.cls} and \textbf{.tex} files that it describes.

	%
	% The following two commands are all you need in the
	% initial runs of your .tex file to
	% produce the bibliography for the citations in your paper.
	\bibliographystyle{abbrv}
	\bibliography{sigproc}  % sigproc.bib is the name of the Bibliography in this case
	% You must have a proper ".bib" file
	%  and remember to run:
	% latex bibtex latex latex
	% to resolve all references
	%
	% ACM needs 'a single self-contained file'!
	%
	%APPENDICES are optional
	%\balancecolumns
	\appendix
	%Appendix A
	\section{Headings in Appendices}
	The rules about hierarchical headings discussed above for
	the body of the article are different in the appendices.
	In the \textbf{appendix} environment, the command
	\textbf{section} is used to
	indicate the start of each Appendix, with alphabetic order
	designation (i.e. the first is A, the second B, etc.) and
	a title (if you include one).  So, if you need
	hierarchical structure
	\textit{within} an Appendix, start with \textbf{subsection} as the
	highest level. Here is an outline of the body of this
	document in Appendix-appropriate form:
	\subsection{Introduction}
	\subsection{The Body of the Paper}
	\subsubsection{Type Changes and  Special Characters}
	\subsubsection{Math Equations}
	\paragraph{Inline (In-text) Equations}
	\paragraph{Display Equations}
	\subsubsection{Citations}
	\subsubsection{Tables}
	\subsubsection{Figures}
	\subsubsection{Theorem-like Constructs}
	\subsubsection*{A Caveat for the \TeX\ Expert}
	\subsection{Conclusions}
	\subsection{Acknowledgments}
	\subsection{Additional Authors}
	This section is inserted by \LaTeX; you do not insert it.
	You just add the names and information in the
	\texttt{{\char'134}additionalauthors} command at the start
	of the document.
	\subsection{References}
	Generated by bibtex from your ~.bib file.  Run latex,
	then bibtex, then latex twice (to resolve references)
	to create the ~.bbl file.  Insert that ~.bbl file into
	the .tex source file and comment out
	the command \texttt{{\char'134}thebibliography}.
	% This next section command marks the start of
	% Appendix B, and does not continue the present hierarchy
	\section{More Help for the Hardy}
	The acm\_proc\_article-sp document class file itself is chock-full of succinct
	and helpful comments.  If you consider yourself a moderately
	experienced to expert user of \LaTeX, you may find reading
	it useful but please remember not to change it.
	\balancecolumns
	% That's all folks!
	\end{document}
